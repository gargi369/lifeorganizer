<?php
namespace AppBundle\Controller;

use Prooph\ServiceBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $web_socket_ip = $this->container->getParameter('web_socket_ip');
        $entryData = [
            'category' => 'cats',
            'title'    => 'new cat race',
            'article'  => 'selkirk rex',
            'when'     => time()
        ];

        $context = new \ZMQContext();
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect("tcp://$web_socket_ip:5555");

        $socket->send(json_encode($entryData));

        return $this->render('default/index.html.twig', []);
    }

    public function socketAction()
    {
        return $this->render('socket.html.twig', []);
    }
}
