<?php

namespace AppBundle\Factory;

use LifeOrganizer\Core\Budget\Event\BudgetCreated;
use LifeOrganizer\Core\Budget\Event\NameChanged;
use LifeOrganizer\Core\Budget\Event\PositionAdded;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\Router\EventRouter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EventBusFactory
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): MessageBus
    {
        $eventBus = new EventBus(
            $this->container->get('prooph_action_event_emitter')
        );
        $budgetProjector = $this->container->get(
            'budget_event_projector_handler'
        );
        $eventRouter = new EventRouter();

        $eventRouter->route(BudgetCreated::class)
            ->to($budgetProjector);
        $eventRouter->route(NameChanged::class)
            ->to($budgetProjector);
        $eventRouter->route(PositionAdded::class)
            ->to($budgetProjector);

        $eventRouter->attachToMessageBus($eventBus);

        return $eventBus;
    }
}
