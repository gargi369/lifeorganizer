<?php

namespace AppBundle\Factory;

use LifeOrganizer\Core\Budget\Query\GetAllBudgets;
use LifeOrganizer\Core\Budget\Query\GetBudgetById;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\Router\QueryRouter;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\DependencyInjection\ContainerInterface;

class QueryBusFactory
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(): MessageBus
    {
        $queryBus = new QueryBus();
        $router = new QueryRouter();

        $router->route(GetAllBudgets::class)
            ->to(
                $this->container->get(
                    'budget.query.handler.get_all_budgets'
                )
            );
        $router->route(GetBudgetById::class)
            ->to(
                $this->container->get(
                    'budget.query.handler.get_budget_by_id'
                )
            );

        $router->attachToMessageBus($queryBus);

        return $queryBus;
    }
}
