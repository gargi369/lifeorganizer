<?php

declare(strict_types=1);

namespace UserBundle\Entity;

final class Username
{
    private $username;

    public function __construct(string $username)
    {
        if (strlen($username) > 25)
        {
            throw new Exception('UsernameTooLongException()');
        }

        if (preg_match('/[^A-Za-z0-9]/', $username))
        {
            throw new Exception('UsernameHasIncorrectCharacterException()');
        }

        $this->username = $username;
    }

    public function username() : string
    {
        return $this->username;
    }
}

