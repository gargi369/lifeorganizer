<?php

declare(strict_types=1);

namespace UserBundle\Entity;

final class Password
{
    private $password;

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function password() : string
    {
        return $this->password;
    }
}

