<?php

declare(strict_types=1);

namespace UserBundle\Entity;

final class Email
{
    private $email;

    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception(); // TODO: wyjątek
        }

        $this->email = $email;
    }

    public function email() : string
    {
        return $this->email;
    }
}

