<?php

namespace BudgetBundle\Infrastructure;

use LifeOrganizer\Core\Budget\BudgetRepository as BaseBudgetRepository;
use LifeOrganizer\Core\Budget\Model\Budget;
use Prooph\EventSourcing\Aggregate\AggregateRepository;
use Prooph\EventSourcing\Aggregate\AggregateType;
use Prooph\EventSourcing\EventStoreIntegration\AggregateTranslator;
use Prooph\EventStore\EventStore;
use Prooph\SnapshotStore\SnapshotStore;

class BudgetRepository extends AggregateRepository implements BaseBudgetRepository
{
    public function __construct(
        EventStore $eventStore,
        SnapshotStore $snapshotStore
    ) {
        parent::__construct(
            $eventStore,
            AggregateType::fromAggregateRootClass(Budget::class),
            new AggregateTranslator(),
            $snapshotStore,
            null,
            true
        );
    }

    public function save(Budget $budget): void
    {
        $this->saveAggregateRoot($budget);
    }

    public function getById(string $id): ?Budget
    {
        /** @var Budget|null $budget */
        $budget = $this->getAggregateRoot($id);

        return $budget;
    }
}
