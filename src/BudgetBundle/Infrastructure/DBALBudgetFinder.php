<?php

namespace BudgetBundle\Infrastructure;

use LifeOrganizer\Core\Budget\Projection\BudgetFinder;
use stdClass;

class DBALBudgetFinder implements BudgetFinder
{
    public function getById(string $id): stdClass
    {
        return new stdClass();
    }

    public function getAll(): array
    {
        return [];
    }
}
