<?php

namespace BudgetBundle\Infrastructure;

use Doctrine\DBAL\Connection;
use LifeOrganizer\Core\Budget\ReadModel\BudgetReadModel as BasicBudgetReadModel;
use Prooph\EventStore\Projection\AbstractReadModel;

class BudgetReadModel extends AbstractReadModel implements BasicBudgetReadModel
{
    const TABLE_NAME = 'read_budget';

    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function init(): void
    {
        $tableName = self::TABLE_NAME;

        $sql = <<<EOT
CREATE TABLE `$tableName` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sum_of_positions` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOT;

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    public function isInitialized(): bool
    {
        $tableName = self::TABLE_NAME;

        $sql = "SHOW TABLES LIKE '$tableName';";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $result = $statement->fetch();

        if (false === $result) {
            return false;
        }

        return true;
    }

    public function reset(): void
    {
        $tableName = self::TABLE_NAME;

        $sql = "TRUNCATE TABLE $tableName;";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    public function delete(): void
    {
        $tableName = self::TABLE_NAME;

        $sql = "DROP TABLE $tableName;";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    public function insert(array $data): void
    {
        $dataCompatibleWithColumnsName = [
            'id' => $data['id'],
            'name' => $data['name'],
            'user_id' => $data['userId'],
            'sum_of_positions' => 0
        ];

        $this->connection->insert(
            self::TABLE_NAME,
            $dataCompatibleWithColumnsName
        );
    }

    public function update(string $id, array $data): void
    {
        $dataCompatibleWithColumnsName = [
            'name' => $data['name'],
            'user_id' => $data['userId']
        ];

        $this->connection->update(
            self::TABLE_NAME,
            $dataCompatibleWithColumnsName,
            [
                'id' => $id
            ]
        );
    }

    public function positionAdded(array $data): void
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder->update(self::TABLE_NAME)
            ->set('sum_of_positions', 'sum_of_positions + :positionValue')
            ->where($queryBuilder->expr()->eq('id', ':id'))
            ->setParameter('positionValue', $data['positionValue'])
            ->setParameter('id', $data['budgetId'])
            ->execute();
    }
}
