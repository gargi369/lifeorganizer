FROM php:7.1-fpm-alpine

ENV ZEROMQ_VERSION 4.1.5
ENV PHP_ZEROMQ_VERSION master

# persistent / runtime deps
ENV PHPIZE_DEPS \
    autoconf \
    cmake \
    file \
    g++ \
    gcc \
    libc-dev \
    pcre-dev \
    make \
    git \
    pkgconf \
    re2c

RUN apk add --no-cache --virtual .persistent-deps \
    zeromq

RUN set -xe \
    && apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        openssl-dev \
        zeromq-dev \
    && pecl install xdebug \
    && pecl install apcu \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-enable apcu \
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-install \
        bcmath \
        pdo_mysql \
        zip \
        opcache \
    && git clone --branch ${PHP_ZEROMQ_VERSION} https://github.com/mkoppanen/php-zmq /tmp/php-zmq \
        && cd /tmp/php-zmq \
        && phpize  \
        && ./configure  \
        && make  \
        && make install \
        && make test \
    && docker-php-ext-enable zmq \
    && apk del .build-deps \
    && rm -rf /tmp/* \
    && rm -rf /var/www \
    && mkdir -p /var/www

WORKDIR /var/www
