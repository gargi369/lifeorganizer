<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171125141434 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->abortCondition(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('CREATE TABLE users (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_1483A5E992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_1483A5E9A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_1483A5E9C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_streams (no BIGINT AUTO_INCREMENT NOT NULL, real_stream_name VARCHAR(150) NOT NULL COLLATE utf8_bin, stream_name CHAR(41) NOT NULL COLLATE utf8_bin, metadata JSON DEFAULT NULL, category VARCHAR(150) DEFAULT NULL COLLATE utf8_bin, UNIQUE INDEX ix_rsn (real_stream_name), INDEX ix_cat (category), PRIMARY KEY(no)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE projections (no BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL COLLATE utf8_bin, position JSON DEFAULT NULL, state JSON DEFAULT NULL, status VARCHAR(28) NOT NULL COLLATE utf8_bin, locked_until CHAR(26) DEFAULT NULL COLLATE utf8_bin, UNIQUE INDEX ix_name (name), PRIMARY KEY(no)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE snapshots (aggregate_id VARCHAR(150) NOT NULL COLLATE utf8_bin, aggregate_type VARCHAR(150) NOT NULL COLLATE utf8_bin, last_version INT NOT NULL, created_at CHAR(26) NOT NULL COLLATE utf8_bin, aggregate_root BLOB DEFAULT NULL, UNIQUE INDEX ix_aggregate_id (aggregate_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->abortCondition(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE event_streams');
        $this->addSql('DROP TABLE projections');
        $this->addSql('DROP TABLE snapshots');
        $this->addSql('DROP TABLE users');
    }

    private function abortCondition(): bool
    {
        return $this->connection->getDatabasePlatform()->getName() !== 'mysql';
    }
}
